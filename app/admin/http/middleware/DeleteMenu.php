<?php
/**
 * 删除菜单中间件
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\http\middleware;
use think\facade\Db; 
use think\helper\Str;

class DeleteMenu
{
	
    public function handle($request, \Closure $next)
    {	
		$data = $request->param();
		$extendInfo = Db::name('extend')->where('extend_id',$data['extend_ids'])->find();
		Db::execute('DROP TABLE if exists '.config('database.connections.mysql.prefix').config('my.create_table_pre').$extendInfo['table_name'] );
		return $next($request);	
		
    }
}
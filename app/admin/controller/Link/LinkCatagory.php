<?php 
/**
 *友情链接分类
*/

namespace app\admin\controller\Link;

use xhadmin\service\admin\Link\LinkCatagoryService;
use xhadmin\db\Link\LinkCatagory as LinkCatagoryDb;
use app\admin\controller\Admin;

class LinkCatagory extends Admin {


	/*友情链接分类*/
	function index(){
		if (!$this->request->isAjax()){
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where['class_name'] = $this->request->param('class_name', '', 'strip_tags,trim');
			$where['status'] = $this->request->param('status', '', 'strip_tags,trim');

			$limit = ($page-1) * $limit.','.$limit;
			try{
				$res = LinkCatagoryService::pageList(formatWhere($where),$limit,$field,$orderby);
				$list = $res['list'];
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}
	}

	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['cata_id']) $this->error('参数错误');
		try{
			LinkCatagoryDb::edit($data);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			return $this->display('add');
		}else{
			$postField = 'class_name,status';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				LinkCatagoryService::add($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$cata_id = $this->request->get('cata_id','','intval');
			if(!$cata_id) $this->error('参数错误');
			$this->view->assign('info',checkData(LinkCatagoryDb::getInfo($cata_id)));
			return $this->display('update');
		}else{
			$postField = 'cata_id,class_name,status';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				LinkCatagoryService::update($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('cata_ids', '', 'strip_tags');
		if(!$idx) $this->error('参数错误');
		try{
			$where['cata_id'] = explode(',',$idx);
			LinkCatagoryService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*查看数据*/
	function view(){
		$cata_id = $this->request->get('cata_id','','intval');
		if(!$cata_id) $this->error('参数错误');
		try{
			$this->view->assign('info',checkData(LinkCatagoryDb::getInfo($cata_id)));
			return $this->display('view');
		} catch (\Exception $e){
			$this->error($e->getMessage());
		}
	}



}


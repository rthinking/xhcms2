<?php 
/**
 *节点列表
*/

namespace app\admin\controller;

use xhadmin\service\admin\NodeService;
use xhadmin\db\Node as NodeDb;

class Node extends Admin {


	/*节点列表*/
	function index(){
		if (!$this->request->isAjax()){
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$orderby = 'sortid asc';
			$limit = ($page-1) * $limit.','.$limit;
			try{
				$res = NodeService::pageList(formatWhere($where),$limit,$field,$orderby);
				$list = $res['list'];
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$list = formartList(['id', 'pid', 'title','title'],$list);
			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}
	}

	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['id']) $this->error('参数错误');
		try{
			NodeDb::edit($data);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			$id = $this->request->get('id','','intval');
			$info = NodeDb::getInfo($id);
			$info['pid'] = $info['id'];
			$this->view->assign('info',$info);
			return $this->display('add');
		}else{
			$postField = 'title,val,pid,status,sortid,is_menu,icon';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				NodeService::add($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$id = $this->request->get('id','','intval');
			if(!$id) $this->error('参数错误');
			$this->view->assign('info',checkData(NodeDb::getInfo($id)));
			return $this->display('update');
		}else{
			$postField = 'id,title,val,pid,status,sortid,is_menu,icon';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				NodeService::update($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('ids', '', 'strip_tags');
		if(!$idx) $this->error('参数错误');
		try{
			$where['id'] = explode(',',$idx);
			NodeService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}



}


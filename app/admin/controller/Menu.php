<?php

namespace app\admin\controller;
use app\admin\service\MenuService;
use app\admin\db\Menu as MenuDb;
use app\admin\db\Application;

class Menu extends Admin
{
	
	
    public function index(){
	   
	   if (!$this->request->isAjax()){
		    $app_id = $this->request->get('app_id','','intval');
			$this->view->assign('app_id',$app_id);
			return $this->display($this->getTpl($app_id,'index'));
		}else{
			$limit  = input('post.limit', 0, 'intval');
			$offset = input('post.offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;
			
			$app_id = $this->request->get('app_id','','intval');

			$limit = ($page-1) * $limit.','.$limit;
			try{
				$res = MenuService::pageList(['app_id'=>$app_id],$limit);
			}catch(\Exception $e){
				exit($e->getMessage());
			}
			$data['rows']  = formartList(['menu_id', 'pid', 'title','cname'],$res['list']);
			$data['total'] = $res['count'];

			return json($data);
		}
    }
	
	public function add(){
		if (!$this->request->isPost()){
			$app_id = $this->request->get('app_id','','intval');
			if(!$app_id) $this->error('参数错误');
			$this->view->assign('menuList',formartList(['menu_id','pid','title','title'],MenuDb::loadList(['app_id'=>$app_id])));
			$this->view->assign('app_id',$app_id);
			return $this->display($this->getTpl($app_id,'info'));
		}else{
			$data = input('post.');
			try{
				MenuService::saveData('add',$data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}
	
	public function update(){
		if (!$this->request->isPost()){
			$menu_id = $this->request->get('menu_id','','intval');
			if(!$menu_id) $this->error('参数错误');
			$info = MenuDb::getInfo($menu_id);
			$this->view->assign('info',$info);
			$this->view->assign('menuList',formartList(['menu_id','pid','title','title'],MenuDb::loadList(['app_id'=>$info['app_id']])));
			$this->view->assign('app_id',$info['app_id']);
			return $this->display($this->getTpl($info['app_id'],'info'));
		}else{
			$data = input('post.');
			try{
				MenuService::saveData('edit',$data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}
	
	//更新排序
	public function setSort(){
		$id = $this->request->post('id','','intval');
		$sortid = $this->request->post('sortid','','intval');
		if(!$id || !$sortid) $this->error('参数错误');

		try{
			MenuDb::edit(['menu_id'=>$id,'sortid'=>$sortid]);
		} catch (\Exception $e) {
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'修改成功']);
	}
	
	//箭头排序
	public function arrowsort(){
		$id = $this->request->post('menu_id','','intval');
		$type = $this->request->post('type','','intval');		
		if(!$id || !$type) $this->error('参数错误');	
		try{
			MenuService::arrowsort($id,$type);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'设置成功']);
	}
	
	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['menu_id']) $this->error('参数错误');
		try{
			MenuDb::edit($data);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}
	
	//卸载业务模块
	public function delete(){
		$menu_id = $this->request->post('menu_id','','intval');
		if(!$menu_id) $this->error('参数错误');
		
		try{
			$res = MenuService::delete($menu_id);
		} catch (\Exception $e) {
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'删除成功']);
	}
	
	//复制菜单
	public function copyMenu(){
		$app_id  = $this->request->post('app_id', '', 'intval');
		$menu_id  = $this->request->post('menu_id', '', 'intval');
		
		if(!$app_id || !$menu_id){
			return json(['status'=>'01','msg'=>'参数错误']);
		}
		
		try{
			$menuInfo = MenuDb::getInfo($menu_id);
			$menuInfo['app_id'] = $app_id;
			$menuInfo['table_status'] = 0;
			$menuInfo['pid'] = 0;
			unset($menuInfo['menu_id']);
			$res = MenuDb::createData($menuInfo);
			
			$fieldList = \app\admin\db\Field::loadList(['menu_id'=>$menu_id]);
			if($fieldList){
				foreach($fieldList as $key=>$val){
					unset($val['id']);
					$val['menu_id'] = $res;
					$val['is_field'] = 0;
					\app\admin\db\Field::createData($val);
				}
			}
			
			$applicationInfo = Application::getInfo($app_id);
			
			$actionList = \app\admin\db\Action::loadList(['menu_id'=>$menu_id]);
			if($actionList){
				foreach($actionList as $key=>$val){
					unset($val['id']);
					$val['menu_id'] = $res;
					$val['log_status'] = 1;
					if($applicationInfo['app_type'] == 2 && $val['type'] <> 6){
						$val['remark'] = '';
					}
					if($val['type'] <> 16){
						\app\admin\db\Action::createData($val);
					}	
				}
			}	
		}catch(\Exception $e){
			return json($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}
	
}
